package s08;

public class HelloWorld2 {

	public static void main(String[] args) {
		/*
		 * Il faut un objet pour appeler une méthode non statique 
		 * à partir d'une méthode statique 
		 */
		HelloWorld2 h= new HelloWorld2();
		h.hello();//appel de la méthode hello

	}
	
	void hello(){//méthode hello ne retourne rien car void
		System.out.print("En Français: ");
		salut();//appel méthode salut
		System.out.println("In English: Hello World!");
	}
	void salut(){//méthode salut
		System.out.println("Salut le Monde!");
	}

}
