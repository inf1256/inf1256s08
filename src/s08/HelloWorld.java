package s08;

public class HelloWorld {

	public static void main(String[] args) {
		/*
		 * Il faut un objet pour appeler une méthode non statique 
		 * à partir d'une méthode statique 
		 */
		HelloWorld h= new HelloWorld();
		h.hello();//appel de la méthode hello

	}
	
	void hello(){//méthode hello ne retourne rien car void
		System.out.println("Hello World!");
	}

}
