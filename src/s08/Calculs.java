package s08;
import java.util.*;
public class Calculs {

	public static void main(String[] args) {
		
		Calculs cal= new Calculs();
		double s = cal.somme(12.9,43.8,45.0);//remplacer . par , si nécessaire
        System.out.println("s ="+s);// peut afficher 101.699999 à cause de la précision machine
        cal.affichage(s);
	}
	
	double somme(double... nombres){//nombre variable d'arguments type double
		double soe = 0.0;
		for(int i=0;i<nombres.length;i++){
			soe = soe + nombres[i];
		}
		return (soe);
	}
	
	void affichage(double nombre){//methode avec 1 paramètre
		System.out.format("%nLa somme = %.3f %n",nombre);
	}

}
