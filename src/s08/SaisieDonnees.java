/**
 * @author Johnny Tsheke @ UQAM --INF1256
 * 2017-03-22 Exemple de saisie de données avec Scanner dans plusieurs méthodes
 */
package s08;

import java.util.*;
public class SaisieDonnees {

	/**
	 * @param args
	 */
	Scanner clavier;
	public static void main(String[] args) {
		SaisieDonnees saisie = new SaisieDonnees();
		saisie.clavier = new Scanner(System.in);
		String pat = saisie.saisirPattern();
		System.out.println("Saisir une chaine respectant le pattern: "+pat);
		String entree = saisie.saisirChaine(pat);
		System.out.println("La chaine saisie est : "+entree);
		
        saisie.clavier.close();
	}
	
	String saisirPattern(){// ne fait pas de boucle
		System.out.println("Veuillez saisir un pattern -- Expression reguliere svp");
		String pat = "";
		try{
			pat = clavier.next();
		}catch(Exception e){
			pat=".";//valeur par défaut
		}
		return(pat);
	}
	
	String saisirChaine(String pattern){
		String chaine="";
		try{
			chaine = clavier.next(pattern);	
		}catch(Exception e){
			//
		}
		return(chaine);
	}

}
